<?php
	//include DB_CONNEXION;
	include PAGE_ERROR;
	include _ROOT.'/libs/html.lib.php';
	include _ROOT.'/classes/Annonce.class.php';	
	include _ROOT.'/classes/Categorie.class.php';

		$existe = \DB\Entities\Annonce::idExist($_GET['id']);

		//$stmt = $bdd->query('SELECT * FROM annonces');
		if (isset($_GET['id']) && $existe){
			/*$stmt = $bdd->prepare('SELECT * FROM annonces A, categories C WHERE A.id_categorie = C.id_categorie AND id_annonce = ?');
			$stmt->execute(array($_GET['id']));*/
			$ANNONCE = \DB\Entities\Annonce::findById($_GET['id']);
			//var_dump($ANNONCE);
		}

		else {
			throw new PageInexistanteException();
		}

		//$ANNONCE = $stmt->fetch();

		/*$stmt_cat = $bdd->query('SELECT * FROM categories');
		$stmt_cat->execute();
		$CATEGORIE = $stmt_cat->fetchAll(PDO::FETCH_ASSOC);*/
		//var_dump($CATEGORIE);

		$CATEGORIE = \DB\Entities\Categorie::findAll();

		//var_dump($ANNONCE);

		\DB\DBConnexion::closeConnection($bdd);

		echo "<form action='?page=annonces/modifier-action-orm' method='POST'>";
			echo "<table style:'width:700px;'>";
				echo "<thead>";
					echo "<tr>Modification de l'annonce n° ".$_GET['id']."</tr>";
				echo "</thead>";
				

				echo "<tbody>";
					echo "<tr>
							<td>DATE</td>
							<td name='date' style='width:100%;'>
								<input type='datetime-local' id='date' name='date' value= '".$ANNONCE->getDate()."' readonly='readonly'>							
							</td>
							<td>
								<input type='hidden' id='id_annonce' name='id_annonce' value= '".$ANNONCE->getIdAnnonce()."'>							
							</td>
							<td>
								<input type='hidden' id='id_categorie' name='id_categorie' value= '".$ANNONCE->getIdCategorie()."'>							
							</td>
							<td>
								<input type='hidden' id='id_utilisateur' name='id_utilisateur' value= '".$ANNONCE->getIdUtilisateur()."'>							
							</td>
						</tr>";
					echo "<tr>
							<td>TITRE *</td>
							<td>
								<input type='text' id='titre' name='titre' value='".htmlspecialchars($ANNONCE->getTitre())."' style='width:100%;' required maxlength='35'>
							</td>	
						</tr>";
					echo "<tr>
							<td>CONTENU *</td>
							<td>
								<textarea type='text' id='contenu' name='contenu' style='width:100%; height:400px; font-family: Trebuchet MS, Arial, sans-serif; font-size:15px;' required >".htmlspecialchars($ANNONCE->getContenu())."</textarea>
							</td>
						</tr>";
					echo "<tr>
							<td>PRIX</td>
							<td>
								<input type='number' id='prix' name='prix' value='".htmlspecialchars($ANNONCE->getPrix())."' style='width:100%;' maxlength='6'>
							</td>
						</tr>";
					echo "<tr>
							<td>CATEGORIE</td>
							<td>
								<select name=categorie>";
									//echo "<option value='' "					
									foreach ($CATEGORIE as $key => $value) {
										echo "<option value=".$value->getIdCategorie()." ".($value->getLibelle()==$ANNONCE->getLibelle()? "selected" : "").">".$value->getLibelle()."</option>";
									}

								echo "</select>
							</td>
						</tr>";
						
						if($ANNONCE->getFilepath()!=NULL){
						
							echo "<tr>
									<td>PHOTO</td>
										<td style='float:left;'>
											<a href='/"._SERVER_PATH."/images/".$ANNONCE->getFilepath()."' ><img src='/"._SERVER_PATH."/images/".$ANNONCE->getFilepath()."' style='max-width: 100px; max-height: 100px;'/></a>
											<input type='hidden' name='filepath' value=".$ANNONCE->getFilepath().">
									</td>
								</tr>";
						}

					echo "<tr>							
							<td>
								<a href='?page=annonces/liste-annonces-orm'><input type=\"button\" value=\"Retour\"></a>
							</td>
							<td style='float:right;'>".
								input_button('Envoyer', 'modif_annonce', '')
							."</td>
						</tr>";								

				echo "</tbody>";
			echo "</table>";
		echo "</form>";

		