<?php 

	//include DB_CONNEXION;	
	include _ROOT.'/libs/html.lib.php';

	$bdd = \DB\DBConnexion::getInstance();


	$stmt = $bdd->query('SELECT * FROM utilisateurs');
	$UTILISATEURS = $stmt->fetchAll() ;

	\DB\DBConnexion::closeConnection($bdd);

	echo "<table>";
		echo "<thead>";
		echo "<tr></tr>";
		echo "</thead>";

		echo "<tbody>";
			echo "<tr>";
				echo "<th>NOM</th>";
				echo "<th>PRENOM</th>";
				echo "<th>ADRESSE</th>";
				echo "<th>TELEPHONE</th>";
				echo "<th></th>";
			echo "</tr>";

				foreach ($UTILISATEURS as $key => $value) {
					echo "<tr>";
						echo "<td>".strtoupper($value['nom'])."</td>";
						echo "<td>".$value['prenom']."</td>";
						echo "<td>".$value['adresse']."</td>";
						echo "<td>".$value['telephone']."</td>";
						echo "<td>".anchor("?page=utilisateurs/utilisateur-modifier&id=".$value['id_utilisateur'], 'modifier', '')."</td>";
					echo "</tr>";
				}

		echo "</tbody>";
		

		echo "</table>";
