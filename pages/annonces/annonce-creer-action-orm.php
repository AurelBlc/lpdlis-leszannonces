<?php 

	//include DB_CONNEXION;
	include _ROOT.'/libs/html.lib.php';
	include _ROOT.'/classes/Annonce.class.php';

	print_r($_FILES);

	$lastID = \DB\Entities\Annonce::lastID();

	$values = 	(['id_annonce' => $_POST['id_annonce'],
				'titre' => $_POST['titre'],
				'contenu' => $_POST['contenu'],
				'prix' => ($_POST['prix']=="")? '0' : $_POST['prix'],
				'id_categorie' => $_POST['categorie'],
				'jour' => $_POST['date'],
				'id_utilisateur' => $_POST['id_utilisateur'],
				'id_annonce' => $lastID;
				]);

	$bdd = \DB\DBConnexion::getInstance();
	
	try {

			$annonce = new \DB_Entities\Annonce();
			$annonce->hydrate($values);
			$annonce->save();

			echo " <h2> L'annonce a bien été créee !</h2><button type=\"submit\" class=\"btn btn-warning\" name=\"retour\"
					onclick=\"parent.location='index.php?page=annonces/liste-annonces'\">Retour aux annonces<span class=\"glyphicon glyphicon-backward\"></span>";

		} else {
			echo " <h2> L'annonce n'a pas été créer : ID incorrect !</h2><button type=\"submit\" class=\"btn btn-warning\" name=\"retour\"
					onclick=\"parent.location='index.php?page=annonces/liste-annonces'\">Retour aux annonces<span class=\"glyphicon glyphicon-backward\"></span>";
		}

		
	} catch (PDOException $e){
		die("erreur de requêre SQL : ".$e->getMessage());
	}

	\DB\DBConnexion::closeConnection($bdd);
	
	