<?php
	//include DB_CONNEXION;
	include _ROOT.'/libs/html.lib.php';

		
		//$bdd = new PDO('mysql:host=localhost;dbname=lps2ima_annonces;charset=utf8mb4', 'root', 'root');
		$bdd = \DB\DBConnexion::getInstance();
		//$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		

		//$stmt = $bdd->query('SELECT * FROM annonces');
		//les exceptions de connexion sont gérées par l'instance de connexion à la base de données.
		//Sinon il faut encadrer les requêtes par une try/catch
		if (isset($_GET['categorie'])){
			$stmt = $bdd->prepare('SELECT * FROM annonces A, categories C WHERE A.id_categorie = C.id_categorie AND C.libelle = ? ORDER BY date DESC');
			$stmt->execute(array($_GET['categorie']));
		}

		else {
			$stmt = $bdd->query('SELECT * FROM annonces ORDER BY date DESC');
		}	

		while ($row = $stmt->fetch()){
				$ANNONCES[] = $row;
		}

		\DB\DBConnexion::closeConnection($bdd);

		if(!empty($ANNONCES)){

			echo "<table>";
			echo "<thead>";
			echo "<tr></tr>";
			echo "</thead>";
			

			echo "<tbody>";
				echo "<tr>";
					echo "<th>DATE</th>";
					echo "<th>TITRE</th>";
					echo "<th>CONTENU</th>";
					echo "<th>PRIX</th>";
					echo "<th></th>";
				echo "</tr>";

					foreach ($ANNONCES as $key => $value) {
						echo "<tr>";
							echo "<td>".$value['date']."</td>";
							echo "<td>".$value['titre']."</td>";
							echo "<td>".substr(htmlspecialchars($value['contenu']),0,150)."</td>";
							echo "<td>".$value['prix']."&euro;</td>";
							echo "<td>".anchor("?page=annonces/annonce-modifier&id=".$value['id_annonce'], 'modifier', '')."</td>";
							//"<td><a href='?page=annonces/annonce-modifier&amp;id=".$row['id_annonce']."'>modifier</td>";
						echo "</tr>";
					}

			echo "</tbody>";
			

			echo "</table>";
		}

		else{
			echo "<div>";
				echo "<h2>Cette rubrique ne comporte aucune annonce...</h2>";
				echo anchor('?page=annonces/liste-annonces', 'Consultez toutes nos annonces', '');
			echo "</div>";
		}


	