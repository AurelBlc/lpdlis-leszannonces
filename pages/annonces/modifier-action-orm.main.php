<?php 

	//include DB_CONNEXION;
	include _ROOT.'/libs/html.lib.php';
	include _ROOT.'/classes/Annonce.class.php';

	print_r($_POST);

	$values = 	(['id_annonce' => $_POST['id_annonce'],
				'titre' => $_POST['titre'],
				'contenu' => $_POST['contenu'],
				'prix' => ($_POST['prix']=="")? '0' : $_POST['prix'],
				'id_categorie' => $_POST['categorie'],
				'jour' => $_POST['date'],
				'id_utilisateur' => $_POST['id_utilisateur'],
				'id_annonce' => $_POST['id_annonce'],
				'filepath' => file_exists($_POST['filepath'])? $_POST['filepath'] : NULL
				]);

	//var_dump("prix = ".$prix);

	//$bdd = new PDO('mysql:host=localhost;dbname=lps2ima_annonces;charset=utf8mb4', 'root', 'root');
	$bdd = \DB\DBConnexion::getInstance();
	//$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	try {
		//var_dump($id);
		//methode qui permet de savoir si l'id de l'annonce que l'on modifie existe bien
		/*$stmt = $bdd->prepare("SELECT count(*) as nb FROM annonces WHERE annonces.id_annonce= :id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$row = $stmt->fetch();
		$result = $row['nb'];*/
		//var_dump($result);

		$result = \DB\Entities\Annonce::idExist($_POST['id_annonce']);

		if( $result!=0){

			/*$stmt = $bdd->prepare("UPDATE annonces SET titre= :titre, contenu= :contenu, prix= :prix, id_categorie= :id_categorie, date= :jour WHERE annonces.id_annonce= :id");
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':prix', $prix);
			$stmt->bindParam(':id_categorie', $id_categorie);
			$stmt->bindParam(':jour', $jour);
			$stmt->bindParam(':id', $id);

			$stmt->execute();*/
			//$stmt->update();

			$annonce = new \DB\Entities\Annonce();
			$annonce->hydrate($values);
			$annonce->save();

			echo " <h2> L'annonce a bien été modifiée !</h2><button type=\"submit\" class=\"btn btn-warning\" name=\"retour\"
					onclick=\"parent.location='index.php?page=annonces/liste-annonces-orm'\">Retour aux annonces<span class=\"glyphicon glyphicon-backward\"></span>";

		} else {
			echo " <h2> L'annonce n'a pas été modifiée : ID incorrect !</h2><button type=\"submit\" class=\"btn btn-warning\" name=\"retour\"
					onclick=\"parent.location='index.php?page=annonces/liste-annonces-orm'\">Retour aux annonces<span class=\"glyphicon glyphicon-backward\"></span>";
		}

		
	} catch (PDOException $e){
		die("erreur de requêre SQL : ".$e->getMessage());
	}

	\DB\DBConnexion::closeConnection($bdd);
	
	