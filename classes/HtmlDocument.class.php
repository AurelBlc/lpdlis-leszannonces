<?php
/**
 * Moteur de template simple.
 *
 * Les pages doivent être stockées dans le répertoire pages/ et porter l'extension '.inc.php'. Une page peut retourner l'exception NotConnectedException si elle requière la connexion de l'utilisateur pour être affichée.
 * Les templates doivent être stockées dans le répertoire templates/. Ce sont des répertoires qui portent le nom du template et doivent contenir le fichier 'template.php' (ainsi que les éventuelles ressources liées au template: images, etc...)
 *
 * Utilisation basique :
 *   $doc = new HtmlDocument('mapage') ;
 *   $doc->applyTemplate('defaut') ;
 *   $doc->render() ;
 */
class HtmlDocument {
	
	static private $currentInstance;

	protected $mainFilePath = ''; //include
	protected $templateName = '';
	protected $headers = array(); //Tableau clé=>valeur qui contient du html
	protected $mainContent=''; //partie du code qui est dynamique
	protected $bodyContent='';

	public function __construct($fileName){
		if (HtmlDocument::$currentInstance !== null){
			throw new Exception('Attention, instance déjà utilisée');
		}
		HtmlDocument::$currentInstance = $this; //accès à un attribut static
		//self::$currentInstance = $this; écriture aussi possible avec le self
		
		$filepath =__DIR__ . '/../pages/'.$fileName.'.main.php';
		if (strpbrk($fileName, '.<>!?:')){
            $filepath =__DIR__ . '/../pages/erreur404.main.php';
            $this->addHeader('<link rel="stylesheet" href="templates/defaut/styles/erreur404.css" />');
        }

        $filepathStyle = __DIR__.'/../templates/defaut/styles/'.$fileName.'.css';

		if(!file_exists($filepath)){
			$filepath =__DIR__ . '/../pages/erreur404.main.php';
			$this->addHeader('<link rel="stylesheet" href="templates/defaut/styles/erreur404.css" />');
		}
        if(file_exists($filepathStyle)){
        	$this->addHeader('<link rel="stylesheet" href="templates/defaut/styles/'.$fileName.'.css" />');
        }

		if(file_exists($filepath)){
			$this->mainFilePath = $filepath;
		}


		$this->parseMain();
		
	}
	/*
	** Fonction qui permet de parser le code HTML dans un buffer
	*/
	protected function parseMain(){
		ob_start();
		try {			
			include $this->mainFilePath;
		} catch (Exception $e) {
			die ($e);
		}
		$this->mainContent = ob_get_clean();
	}
	protected function parseTemplate(){
		$pathTemplate = __DIR__.'/../templates/'.$this->templateName.'/template.php';
		ob_start();
		include $pathTemplate;
		$this->mainContent = ob_get_clean();
	}

	public function applyTemplate($templateName){
		$this->templateName = $templateName;
		$this->parseTemplate();
	}
	public function render(){
		
        $this->addHeader('<meta charset="utf-8" />');
        $this->addHeader('<link rel="stylesheet" href="templates/defaut/styles/style.css" />');
        $this->addHeader('<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->');
        $this->addHeader('<title>Les Z\'Annonces</title>');
    	
    	echo '<html>';
	    	echo '<head>';
		    	foreach ($this->headers as $row) {
					echo $row;
				}
				echo $this->mainContent;
	    	echo '</head>';
    	echo '</html>';

		
	}

	public function addHeader($html){
		array_push($this->headers, $html);
	}

	public function getMainContent(){
		return $this->mainContent;
	}

	static function getCurrentInstance(){
		return HtmlDocument::$currentInstance;
	}

}
