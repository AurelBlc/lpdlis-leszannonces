<?php
// Inclusion des classes et librairies utilisées
require 'classes/HtmlDocument.class.php' ;
require 'libs/mobile.lib.php' ;
require 'classes/DBConnexion.class.php';

define("_ROOT", __DIR__);
define("_SERVER_PATH", basename(__DIR__));
define("DB_CONNEXION", _ROOT.'/classes/DBConnexion.php');
define("PAGE_ERROR", _ROOT.'/classes/PageInexistanteException.php');


// Pour is_mobile()
// Valeur par défaut pour la page si non renseignée
$page = isset($_GET['page']) ? $_GET['page'] : 'index' ;
// Appel au constructeur : il lit le corps de la page et le mémorise dans la variable
//interne $mainContent
$doc = new HtmlDocument($page) ;
// Application du template désiré : on lit le fichier de template (ce dernier
//utilisera la fonction getMainContent() pour inclure le corps de la page à l'endroit
//souhaité) et on stoque le résultat dans la variable interne $bodyContent (= ce qu'il
// a entre les balises <body> du document HTML)
$doc->applyTemplate( is_mobile() ? 'mobile' : 'defaut' ) ;
// On envoie au navigateur le code HTML complet de la page. C'est à ce stade que l'on
//génère la balise <html> mais aussi <head> pour inclure les styles CSS entre autre.
$doc->render() ;
?>