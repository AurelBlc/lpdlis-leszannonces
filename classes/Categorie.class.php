<?php

	namespace DB\Entities{

		require_once (__DIR__.'/DBConnexion.class.php');
		require_once (__DIR__.'/Entity.class.php');
		use PDO;

		Class Categorie extends \DB\Entity{

			private const TABLENAME = 'categories';
			private const PKNAME = 'id_categorie';
			private $VALUES = [];

			public function __construct(){
				parent::__construct(self::TABLENAME, self::PKNAME);
				//echo self::TABLENAME;
			}

			public static function findAll(){
				$liste = [];
				//$cat = new Categorie();
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT * FROM ". self::TABLENAME);
				//$stmt = $bdd->prepare("SELECT * FROM categories");
				$stmt->execute();

				while ($rows = $stmt->fetch(PDO::FETCH_ASSOC)){
					$cat = new Categorie();
					//var_dump($rows);
					$cat->hydrate($rows);
					array_push($liste, $cat);
				}
				return $liste;				
			}

			public static function findById($id){
				$cat = new Categorie();
				$bdd = \DB\DBConnexion::getInstance();

				$stmt = $bdd->prepare("SELECT * FROM ". self::TABLENAME. " WHERE ".self::PKNAME."=".$id);
				$stmt->execute();

				$row = $stmt->fetch();
				$cat->hydrate($row);

				return $cat;				
			}

			public static function idExist($id){
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT * FROM ".self::TABLENAME." WHERE ".self::TABLENAME.".".self::PKNAME."=:id");
				$stmt->bindParam(':id', $id);

				$stmt->execute();

				$existe = $stmt->fetch();

				return $existe;
			}
		}
	}