<?php

	//include DB_CONNEXION;	
	include _ROOT.'/libs/html.lib.php';
	include _ROOT.'/classes/Utilisateur.class.php';

	//print_r($_POST);

	$existe = \DB\Entities\Utilisateur::idExist($_GET['id']);

	if (isset($_GET['id']) && $existe){
			$UTILISATEUR = \DB\Entities\Utilisateur::findById($_GET['id']);
			//var_dump($UTILISATEUR);
	}

	else {
		throw new PageInexistanteException();
	}

	if(isset($_POST['modif_user'])){
		//print_r($_POST);
		$values = 	(['id_utilisateur' => $_POST['id_utilisateur'],
					'nom' => $_POST['nom'],
					'prenom' => $_POST['prenom'],
					'adresse' => $_POST['adresse'],
					'telephone' => $_POST['telephone']
					]);
		$user = new \DB\Entities\Utilisateur();
		$user->hydrate($values);
		$user->save();
		$UTILISATEUR = $user;
	}

	//var_dump($UTILISATEUR);
	//var_dump(idExist($_GET['id'], $bdd));

	\DB\DBConnexion::closeConnection($bdd);

	echo "<form action='?page=utilisateurs/utilisateur-modifier-orm&id=".$_GET['id']."' method='POST'>";
			echo "<table style:'width:700px;'>";
				echo "<thead>";
					echo "<tr>Mise à jour de l'utilisateur</tr>";
				echo "</thead>";
				

				echo "<tbody>";
					echo "<tr style='width:100%'>
							<td>NOM *</td>
							<td style='width:100%'>
								<input type='text' id='nom' name='nom' value= '".htmlspecialchars(strtoupper($UTILISATEUR->getNom()))."' required style='width:100%'>				
							</td>
							<td style='width:100%'>
								<input type='hidden' id='id_utilisateur' name='id_utilisateur' value= '".($UTILISATEUR->getIdUtilisateur())."' required style='width:100%'>				
							</td>
						</tr>";
					echo "<tr>
							<td>PRENOM *</td>
							<td>
								<input type='text' id='prenom' name='prenom' value= '".htmlspecialchars($UTILISATEUR->getPrenom())."' style='width:100%;' required maxlength='35'>
							</td>	
						</tr>";
					echo "<tr>
							<td>ADRESSE *</td>
							<td>
								<input type='text' id='adresse' name='adresse' value='".htmlspecialchars($UTILISATEUR->getAdresse())."' style='width:100%;' required>
							</td>
						</tr>";
					echo "<tr>
							<td>TELEPHONE *</td>
							<td>
								<input type='tel' id='telephone' name='telephone' value='".htmlspecialchars($UTILISATEUR->getTelephone())."' style='width:100%;' maxlength='10' required>
							</td>
						</tr>";
					echo "<tr>
							<td>
								<a href='?page=utilisateurs/liste-utilisateurs-orm'><input type=\"button\" value=\"Retour\"></a>
							</td>
							<td style='float:right';>".
								input_button('Envoyer', 'modif_user', '')
							."</td>
						</tr>";
					if(isset($_POST['modif_user'])){
						echo "<tr>
							<td></td>
							<td>Informations du membre mises à jour</td>
						</tr>";
					}
				echo "</tbody>";
			echo "</table>";
		echo "</form>";

		



