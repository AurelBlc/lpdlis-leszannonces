<?php 

	//include DB_CONNEXION;	
	include _ROOT.'/libs/html.lib.php';
	include _ROOT.'/classes/Utilisateur.class.php';

	$UTILISATEURS = \DB\Entities\Utilisateur::findAll();

	\DB\DBConnexion::closeConnection($bdd);

	echo "<table>";
		echo "<thead>";
		echo "<tr></tr>";
		echo "</thead>";

		echo "<tbody>";
			echo "<tr>";
				echo "<th>NOM</th>";
				echo "<th>PRENOM</th>";
				echo "<th>ADRESSE</th>";
				echo "<th>TELEPHONE</th>";
				echo "<th></th>";
			echo "</tr>";

				foreach ($UTILISATEURS as $key => $utilisateur) {
					
					echo "<tr>";
						echo "<td>".strtoupper($utilisateur->getNom())."</td>";
						echo "<td>".$utilisateur->getPrenom()."</td>";
						echo "<td>".$utilisateur->getAdresse()."</td>";
						echo "<td>".$utilisateur->getTelephone()."</td>";
						echo "<td>".anchor("?page=utilisateurs/utilisateur-modifier-orm&id=".$utilisateur->getIdUtilisateur(), 'modifier', '')."</td>";
					echo "</tr>";
				}

		echo "</tbody>";
		

		echo "</table>";
