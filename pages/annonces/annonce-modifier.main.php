<?php
	//include DB_CONNEXION;
	include PAGE_ERROR;
	include _ROOT.'/libs/html.lib.php';


		//$bdd = new PDO('mysql:host=localhost;dbname=lps2ima_annonces;charset=utf8mb4', 'root', 'root');
		$bdd = \DB\DBConnexion::getInstance();
		//$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		function idExist($id, $bdd){
			$stmt = $bdd->prepare("SELECT * FROM annonces WHERE annonces.id_annonce=:id");
			$stmt->bindParam(':id', $id);

			$stmt->execute();

			$existe = $stmt->fetch();

			return $existe;
		}

		//$stmt = $bdd->query('SELECT * FROM annonces');
		if (isset($_GET['id']) && idExist($_GET['id'], $bdd)){
			$stmt = $bdd->prepare('SELECT * FROM annonces A, categories C WHERE A.id_categorie = C.id_categorie AND id_annonce = ?');
			$stmt->execute(array($_GET['id']));
		}

		else {
			throw new PageInexistanteException();
		}

		$ANNONCE = $stmt->fetch();

		$stmt_cat = $bdd->query('SELECT * FROM categories');
		$stmt_cat->execute();
		$CATEGORIE = $stmt_cat->fetchAll(PDO::FETCH_ASSOC);
		//var_dump($CATEGORIE);

		\DB\DBConnexion::closeConnection($bdd);

		echo "<form action='?page=annonces/modifier-action' method='POST'>";
			echo "<table style:'width:700px;'>";
				echo "<thead>";
					echo "<tr>Modification de l'annonce n° ".$ANNONCE['id_annonce']."</tr>";
				echo "</thead>";
				

				echo "<tbody>";
					echo "<tr>
							<td>DATE</td>
							<td name='date' style='width:100%;'>
								<input type='datetime-local' id='date' name='date' value= '".$ANNONCE['date']."' readonly='readonly'>							
							</td>
							<td>
								<input type='hidden' id='id_annonce' name='id_annonce' value= '".$ANNONCE['id_annonce']."'>							
							</td>
							<td>
								<input type='hidden' id='id_categorie' name='id_categorie' value= '".$ANNONCE['id_categorie']."'>							
							</td>
						</tr>";
					echo "<tr>
							<td>TITRE *</td>
							<td>
								<input type='text' id='titre' name='titre' value='".htmlspecialchars($ANNONCE['titre'])."' style='width:100%;' required maxlength='35'>
							</td>	
						</tr>";
					echo "<tr>
							<td>CONTENU *</td>
							<td>
								<textarea type='text' id='contenu' name='contenu' style='width:100%; height:400px;' required>".htmlspecialchars($ANNONCE['contenu'])."</textarea>
							</td>
						</tr>";
					echo "<tr>
							<td>PRIX</td>
							<td>
								<input type='number' id='prix' name='prix' value='".htmlspecialchars($ANNONCE['prix'])."' style='width:100%;' maxlength='6'>
							</td>
						</tr>";
					echo "<tr>
							<td>CATEGORIE</td>
							<td>
								<select name=categorie>";
									//echo "<option value='' "					
									foreach ($CATEGORIE as $key => $value) {
										echo "<option value=".$value['id_categorie']." ".($value['libelle']==$ANNONCE['libelle']? "selected" : "").">".$value['libelle']."</option>";
									}

								echo "</select>
							</td>
						</tr>";
						//form_select($name, $values, $id_selected=null, $padding='')
					echo "<tr>
							<td></td>
							<td style='float:right;'>".
								input_button('Envoyer', 'modif_annonce', '')
							."</td>
						</tr>";

				echo "</tbody>";
			echo "</table>";
		echo "</form>";

		//$stmt->closeCursor();

		