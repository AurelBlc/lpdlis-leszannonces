<?php 
include _ROOT.'/libs/html.lib.php';

 ?>
<section>
    <article>
        <h1>Vous avez forcément quelque chose à vendre !</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec sagittis massa. Nulla facilisi. Cras id arcu lorem, et semper purus. Cum sociis natoque pens. Mauris in pretium urna. Cras laoreet molestie odio, consequat consequat velit commodo eu. Integer vitae lectus ac nunc posuere pellentesque non at eros. Suspendisse non lectus lorem.</p>
        <p>Vivamus sed libero nec mauris pulvinar facilisis ut non sem. Quisque mollis ullamcorper diam vel faucibus. Vestibulum sollicitudin facilisis feugiat. Nulla euismod sodales he. Duis risus elit, venenatis vel rutrum in, imperdiet in quam. Sed vestibulum, libero ut bibendum consectetur, eros ipsum ultrices nisl, in rutrum diam augue non tortor. Fusce nec massa et risus dapibus aliquam vitae nec diam.</p>
    </article>  
    <article>
        <h1>Toutes nos catégories</h1>

        <ul id='categories'>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Emploi', 'EMPLOI', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Offres d\'emploi', 'Offres d\'emploi', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Offres d\'emploi Cadres', 'Offres d\'emploi Cadres', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=véhicules', 'VEHICULES', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Voitures', 'Voitures', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Motos', 'Motos', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Caravaning', 'Caravaning', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Utilitaires', 'Utilitaires', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Camions', 'Camions', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Nautisme', 'Nautisme', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Equipement Auto', 'Equipement Auto', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Equipement Moto', 'Equipement Moto', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Equipement Caravaning', 'Equipement Caravaning', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Equipement Nautisme', 'Equipement Nautisme', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=IMMOBILIER', 'IMMOBILIER', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Ventes immobilières', 'Ventes immobilières', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Immobilier Neuf', 'Immobilier Neuf', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Locations', 'Locations', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Colocations', 'Colocations', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Bureaux & Commerces', 'Bureaux & Commerces', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=VACANCES', 'VACANCES', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Locations & Gîtes', 'Locations & Gîtes', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Chambres d\'hôtes', 'Chambres d\'hôtes', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Campings', 'Campings', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Hôtels', 'Hôtels', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Hébergements insolites', 'Hébergements insolites', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Ventes privées vacances', 'Ventes privées vacances', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=MAISON', 'MAISON', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Ameublement', 'Ameublement', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Electroménager', 'Electroménager', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Arts de la table', 'Arts de la table', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Décoration', 'Décoration', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Linge de maison', 'Linge de maison', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Bricolage', 'Bricolage', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Jardinage', 'Jardinage', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=MODE', 'MODE', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Vêtements', 'Vêtements', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Chaussures', 'Chaussures', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Accessoires & Bagagerie', 'Accessoires & Bagagerie', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Montres & Bijoux', 'Montres & Bijoux', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Equipement bébé', 'Equipement bébé', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Vêtements bébé', 'Vêtements bébé', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Luxe et Tendance', 'Luxe et Tendance', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=MULTIMEDIA', 'MULTIMEDIA', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Informatique', 'Informatique', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Consoles & Jeux vidéo', 'Consoles & Jeux vidéo', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Image & Son', 'Image & Son', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Téléphonie', 'Téléphonie', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=LOISIRS', 'LOISIRS', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=DVD / Films', 'DVD / Films', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=CD / Musique', 'CD / Musique', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Livres', 'Livres', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Animaux', 'Animaux', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Vélos', 'Vélos', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Sports & Hobbies', 'Sports & Hobbies', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Instruments de musique', 'Instruments de musique', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Collection', 'Collection', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Jeux & Jouets', 'Jeux & Jouets', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Vins & Gastronomie', 'Vins & Gastronomie', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=MATERIEL PROFESSIONNEL', 'MATERIEL PROFESSIONNEL', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Matériel Agricole', 'Matériel Agricole', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Transport - Manutention', 'Transport - Manutention', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=BTP - Chantier Gros-oeuvre', 'BTP - Chantier Gros-oeuvre', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Outillage - Matériaux 2nd-oeuvre', 'Outillage - Matériaux 2nd-oeuvre', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Équipements Industriels', 'Équipements Industriels', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Restauration - Hôtellerie', 'Restauration - Hôtellerie', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Fournitures de Bureau', 'Fournitures de Bureau', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Commerces & Marchés', 'Commerces & Marchés', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Matériel Médical', 'Matériel Médical', '')?></li>
        </ul></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=SERVICES', 'SERVICES', '')?><ul>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Prestations de services', 'Prestations de services', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Billetterie', 'Billetterie', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Evénements', 'Evénements', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Cours particuliers', 'Cours particuliers', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Covoiturage', 'Covoiturage', '')?></li>
        <li><?= anchor('?page=annonces/liste-annonces&categorie=Autres', 'Autres', '')?></li>
        </ul></li>
    </article>
</section>