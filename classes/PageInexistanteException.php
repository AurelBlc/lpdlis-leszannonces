<?php
include _ROOT.'/libs/http.lib.php';

class PageInexistanteException extends Exception { 

	public function __construct(){
		http_redirect('?page=erreur404', '404', 'false');
	}
}