<?php

 	namespace DB{

 		//require 'DBConnexion.class.php';

 		class Entity{

 			protected $tableName;
 			protected $pkName;
 			protected $value;//tableau associatif contenant les données de l'objet. Valeurs contenues en base

 			public function __construct($tableName, $pkName){
 				$this->tableName = $tableName;
 				$this->pkName = $pkName;
 			}

 			/*
 			* Méthode qui alimente l'attribut value avec les valeurs passées en paramètres.
 			*/
 			public function hydrate($value){
 				$this->value = $value;
 			}
 			/*
 			* Méthode appelée lorsqu'une fonction non définie est appliquée sur l'objet (getters et setters)
 			*/
 			public function __call($method, $params){
 				$fonction = substr($method,0,3); //get ou set
 				$attribut = substr($method,3);
 				$attribut = preg_replace("/[A-Z]/", '_$0', $attribut);
 				$attribut = trim(strtolower($attribut), "_");

 				//var_dump($attribut);

 				if($fonction=='set'){
 					$this->value[$attribut] = $params;
 				}

 				else if ($fonction=='get'){
 					//var_dump($this->value);
 					return $this->value[$attribut];

 				}

 				else{
 					throw new UnknownMethodException();
 				}

 			}
 			/*
 			* Méthode génère et execute une méthode préparée
 			*/
 			public function save(){
 				$attributs = array_keys($this->value);
 				$values=[];
 				foreach ($attributs as $key => $val) {
 					array_push($values, $val."=:".$val);
 				}

 				//Crée un tableau sans la PK de la table
 				$valuesDuplicate=[];
 				foreach ($attributs as $key => $val) {
 					if($val != $this->pkName){
 						array_push($valuesDuplicate, $val."=:".$val);
 					}

 				}

 				$bdd = DBConnexion::getInstance();

 				$sql = "INSERT INTO ".$this->tableName." (".implode(", ",$attributs).") VALUES (:".implode(", :",$attributs).") ON DUPLICATE KEY UPDATE ".implode(", ",$valuesDuplicate);

 				//echo $sql;
 				try {
 					$stmt = $bdd->prepare($sql);
					$stmt->execute($this->value);	
 					
 				} catch (PDOException $e) {
 					echo $e->getMessage();
 				}

 				//var_dump($this->value);
 			}

 			public function delete($id){

 				$bdd = DBConnexion::getInstance();
 				$stmt = $bdd->prepare("DELETE FROM ".$this->tableName. " WHERE ".$this->pkName."=".$id);
 				$stmt->execute();

 			}
 		}
 	}