<?php 

	//include DB_CONNEXION;
	include _ROOT.'/libs/html.lib.php';
	include _ROOT.'/classes/Annonce.class.php';	

	//echo sys_get_temp_dir() . PHP_EOL;

	//var_dump ($_FILES['upload']);

	$lastID = \DB\Entities\Annonce::lastID();

	
	try {
		if(!empty($_FILES['upload']['tmp_name'])){

			// on récupère les infos du fichier à uploader			
			$name = $_FILES['upload']['name'];
			
			//dossier temporaire
			$uploadTemp = _ROOT.'/templates/defaut/images';
			
			//création du chemin de l'image
			$file = $uploadTemp . basename($name);
			//deplacement de l'image en $_FILES vers le fichier temporaire
			move_uploaded_file($_FILES['upload']['tmp_name'], $file);

			//Fonction qui asssemble le logo avec l'image uploadée
			\DB\Entities\Annonce::imageMerger($file, $name);

		}

		$values = 	(['titre' => $_POST['titre'],
			'contenu' => $_POST['contenu'],
			'prix' => ($_POST['prix']=="")? '0' : $_POST['prix'],
			'id_categorie' => $_POST['categorie'],
			'date' => date('Y/m/j H:i:s'),
			'id_utilisateur' => $_POST['id_utilisateur'],
			'id_annonce' => $lastID,
			'filepath' => ((!empty($_FILES['upload']['tmp_name']))? $name : NULL)
			]);

		//var_dump($values);

		$annonce = new \DB\Entities\Annonce();
		$annonce->hydrate($values);
		$annonce->save();

		echo " <h2> L'annonce a bien été créée !</h2><button type=\"submit\" class=\"btn btn-warning\" name=\"retour\"
				onclick=\"parent.location='index.php?page=annonces/liste-annonces-orm'\">Retour aux annonces<span class=\"glyphicon glyphicon-backward\"></span>";
	
	} catch (PDOException $e){
		echo " <h2> L'annonce n'a pas été créée : ID incorrect !</h2><button type=\"submit\" class=\"btn btn-warning\" name=\"retour\"
					onclick=\"parent.location='index.php?page=annonces/liste-annonces-orm'\">Retour aux annonces<span class=\"glyphicon glyphicon-backward\"></span>";
	}

	\DB\DBConnexion::closeConnection($bdd);
	
	