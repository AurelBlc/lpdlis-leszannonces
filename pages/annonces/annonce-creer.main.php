<?php
	//include DB_CONNEXION;
	include PAGE_ERROR;
	include _ROOT.'/libs/html.lib.php';	
	include _ROOT.'/classes/Categorie.class.php';
	include _ROOT.'/classes/Utilisateur.class.php';


		//$bdd = new PDO('mysql:host=localhost;dbname=lps2ima_annonces;charset=utf8mb4', 'root', 'root');
		//$bdd = \DB\DBConnexion::getInstance();
		//$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$CATEGORIE = \DB\Entities\Categorie::findAll();
		$UTILISATEURS = \DB\Entities\Utilisateur::findAll();

		echo "<form action='?page=annonces/annonce-creer-action-orm' method='POST' enctype='multipart/form-data'>";
			echo "<table style:'width:700px;'>";
				echo "<thead>";
					echo "<tr>Creation d'une nouvelle annonce</tr>";
				echo "</thead>";
				

				echo "<tbody>";
				echo "<tr>
							<td>MEMBRE *</td>
							<td style='width:100%''>
								<select name=id_utilisateur>";
								echo "<option selected required>Sélectionner un utilisateur</option>";
									//echo "<option value='' "					
									foreach ($UTILISATEURS as $key => $value) {
										echo "<option id='id_utilisateur' name='id_utilisateur' value=".$value->getIdUtilisateur()." style='width:100%;' required >".strtoupper($value->getNom())." ".$value->getPrenom()."</option>";
									}

								echo "</select>
							</td>
						</tr>";
					echo "<tr>
							<td>TITRE *</td>
							<td>
								<input type='text' id='titre' name='titre' value='' style='width:100%;' required maxlength='35'>
							</td>	
						</tr>";
					echo "<tr>
							<td>CONTENU *</td>
							<td>
								<textarea type='text' id='contenu' name='contenu' style='width:100%; height:400px;' required>
								</textarea>
							</td>
						</tr>";
					echo "<tr>
							<td>PRIX</td>
							<td>
								<input type='number' id='prix' name='prix' value='' style='width:100%;' maxlength='6'>
							</td>
						</tr>";
					echo "<tr>
							<td>CATEGORIE</td>
							<td>
								<select name=categorie>";
									//echo "<option value='' "					
									foreach ($CATEGORIE as $key => $value) {
										echo "<option id='categorie' value=".$value->getIdCategorie().">".$value->getLibelle()."</option>";
									}

								echo "</select>
							</td>
						</tr>";
					echo "<tr>
							<td>PHOTO</td>
							<td>
								<input type='file' id='upload' name='upload'>
							</td>
						</tr>";
						//form_select($name, $values, $id_selected=null, $padding='')
					echo "<tr>
							<td></td>
							<td style='float:right;'>".
								input_button('Envoyer', 'ajout_annonce', '')
							."</td>
						</tr>";
		
				echo "</tbody>";
			echo "</table>";
		echo "</form>";

		//$stmt->closeCursor();

		