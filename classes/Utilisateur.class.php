<?php

	namespace DB\Entities{

		require_once (__DIR__.'/DBConnexion.class.php');
		require_once (__DIR__.'/Entity.class.php');
		use PDO;

		Class Utilisateur extends \DB\Entity{

			private const TABLENAME = 'utilisateurs';
			private const PKNAME = 'id_utilisateur';
			

			public function __construct(){
				parent::__construct(self::TABLENAME, self::PKNAME);
				//echo self::TABLENAME;
			}

			public static function findAll(){
				$liste = [];
				//$user = new Utilisateur();
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT * FROM ". self::TABLENAME);
				//$stmt = $bdd->prepare("SELECT * FROM utilisateurs");
				$stmt->execute();

				while ($rows = $stmt->fetch(PDO::FETCH_ASSOC)){
					$user = new Utilisateur();
					//var_dump($rows);
					$user->hydrate($rows);
					array_push($liste, $user);
				}
				return $liste;				
			}

			public static function findById($id){
				$user = new Utilisateur();
				$bdd = \DB\DBConnexion::getInstance();

				$stmt = $bdd->prepare("SELECT * FROM ". self::TABLENAME. " WHERE ".self::PKNAME."=".$id);
				$stmt->execute();

				$row = $stmt->fetch();
				$user->hydrate($row);

				/*echo("************ROW**********");
				var_dump($row);
				echo("************USER**********");
				var_dump($user);*/

				return $user;				
			}

			public static function idExist($id){
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT * FROM ".self::TABLENAME." WHERE ".self::TABLENAME.".".self::PKNAME."=:id");
				$stmt->bindParam(':id', $id);

				$stmt->execute();

				$existe = $stmt->fetch();

				return $existe;
			}
		}
	}