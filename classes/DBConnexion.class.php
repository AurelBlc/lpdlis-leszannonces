<?php
namespace DB{

    use PDOException;

    class DBConnexion extends \PDO{
        static function getInstance() {
            static $dbh = NULL;
            if ($dbh==NULL) {
                $dsn = "mysql:host=localhost;dbname=lps2ima_annonces;charset=utf8mb4";
                $username = "admin";
                $password = "admin";
                $options = array (
                    \PDO::ATTR_ERRMODE, 
                    \PDO::ERRMODE_EXCEPTION,
                    \PDO::ERRMODE_WARNING
                );
                try {
                    $dbh = new \PDO ( $dsn, $username, $password, $options );
                } catch ( PDOException $e ) {
                    echo "Problème de connexion", $e;
                }
            }
            return $dbh;
        }

        static function closeConnection(&$dbh) {
            $dbh=null;
        }
    }
}
