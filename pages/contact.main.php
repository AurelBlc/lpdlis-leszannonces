<section>
	<h1 class="contact-title">Contactez nous !</h1>
	<div class="contact_container">
		<form action="">
			<input class="contact_input_radio" type="radio" id="femme" name="gender" value="femme"><label for="femme">Madame</label>
			<input class="contact_input_radio" type="radio" id="homme" name="gender" value="homme"><label for="femme">Monsieur</label>
			<div class="contact_info">
				<input class="contact_info_input" type="text" id="nom" name="nom" required placeholder="Nom*">
	       		<input class="contact_info_input" type="text" id="prenom" name="prenom" required placeholder="Prénom*">
	       	</div>
	       	<div class="contact_mail">
	       		<input class="contact_mail_input" type="email" id="mail" name="mail" required placeholder="Mail de contact*">
	       	</div>
	       	<div class="contact_message_container">
	       		<input class="contact_message_input" type="text" id="message" name="message" required placeholder="Si vous le désirez, ajoutez un commentaire">
	       	</div>
	       	<div class="contact_rgpd">
	       		<input type="checkbox" id="rgpd" name="rgpd" value="rgpd" OnClick="checkbox();"><label for="rgpd">J'accepte que mes données saisies soient utilisées dans le cadre de ma demande et de la relation commerciale qui peut en découler. *</label>
	       	</div>
	       	<div class="contact_bouton">
	       		<button id="contact_button" name="submit" disabled>Soumettre</button>
	       	</div>
		</form>
	</div>
</section>

<script type="text/javascript">
    function checkbox(){
    	var elmt = document.getElementById("contact_button");

        if(document.getElementById('rgpd').checked){
		elmt.disabled = '';
		// on modifie son style
		elmt.style.backgroundColor = "red";
		
        }
        else{
			elmt.disabled = 'disabled';
			elmt.style.backgroundColor = "grey";
        }
    }
</script>