<?php

	namespace DB\Entities{

		require_once (__DIR__.'/DBConnexion.class.php');
		require_once (__DIR__.'/Entity.class.php');
		use PDO;

		Class Annonce extends \DB\Entity{

			private const TABLENAME = 'annonces';
			private const PKNAME = 'id_annonce';
			private $VALUES = [];

			public function __construct(){
				parent::__construct(self::TABLENAME, self::PKNAME);
				//echo self::TABLENAME;
			}

			public static function findAll(){
				$liste = [];
				//$user = new Utilisateur();
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT * FROM ". self::TABLENAME." ORDER BY date DESC");
				$stmt->execute();

				while ($rows = $stmt->fetch(PDO::FETCH_ASSOC)){
					$annonces = new Annonce();
					//var_dump($rows);
					$annonces->hydrate($rows);
					array_push($liste, $annonces);
				}
				return $liste;				
			}

			public static function findById($id){
				$annonce = new Annonce();
				$bdd = \DB\DBConnexion::getInstance();
				$sql = "SELECT * FROM categories, ". self::TABLENAME. " WHERE categories.id_categorie=".self::TABLENAME.".id_categorie AND ".self::PKNAME."=".$id;

				//echo $sql;
				$stmt = $bdd->prepare($sql);
				//var_dump($stmt);
				$stmt->execute();

				$row = $stmt->fetch();
				$annonce->hydrate($row);

				return $annonce;				
			}

			public static function findByCategory($id_categorie){
				$liste = [];
				//$user = new Utilisateur();
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare('SELECT * FROM '.self::TABLENAME.'categories WHERE '.self::TABLENAME.'.'.self.PKNAME.'= C.'.self::PKNAME.' AND C.libelle =:id_categorie ORDER BY date DESC');
				$stmt->bindParam(':id', $id);
				$stmt->execute();

				while ($rows = $stmt->fetch(PDO::FETCH_ASSOC)){
					$annonces = new Annonce();
					//var_dump($rows);
					$annonces->hydrate($rows);
					array_push($liste, $annonces);
				}
				return $liste;				
			}

			public static function idExist($id){
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT * FROM ".self::TABLENAME." WHERE ".self::TABLENAME.".".self::PKNAME."=:id");
				$stmt->bindParam(':id', $id);

				$stmt->execute();

				$existe = $stmt->fetch();

				return $existe;
			}

			public static function lastId(){
				$bdd = \DB\DBConnexion::getInstance();
				$stmt = $bdd->prepare("SELECT max(".self::PKNAME.") FROM ".self::TABLENAME);

				$stmt->execute();

				$res = $stmt->fetch() ;

				$id = $res[0] + 1;

				return $id;
			}

			public function imageMerger($image, $name){

				//chemin d'accès du logo
				$file_logo = _ROOT.'/templates/defaut/images/logo-ubs.png';
				
				//dossier final
				$filepathServeur = _ROOT."/images/";

				$path = pathinfo($image);
				$extension = $path['extension'];

				//print_r ($path);
				//echo $extension;

				if ($extension=='jpg' || $extension=='jpeg'){
					$dest = imagecreatefromjpeg($image);
				}
				elseif ($extension=='png') {
					$dest = imagecreatefrompng($image);
				}
				$source = imagecreatefrompng($file_logo); //crée une image depuis un fichier ou une URL
				
				$w_dest = imagesx($dest);//récupère les dimensions de l'image
				$h_dest = imagesy($dest);

				$w_source = imagesx($source);
				$h_source = imagesy($source);

				list($max_w, $max_h) = Annonce::image_resized_dims(50,50,$w_source,$h_source); // list() permet de lier les valeurs de 2 tableaux de même dimension

				$new_image = imagecreatetruecolor($max_w, $max_h); //crée une image en couleurs vraies

				imagecopyresampled($new_image, $source, 0, 0, 0, 0, $max_w, $max_h, $w_source, $h_source); //copie, redimensionne, rééchnatillonne une image
				
				imagecopymerge($dest, $new_image, $w_dest-$max_w-10, $h_dest-$max_h-10, 0, 0, 50, 50, 50);
				
				imagejpeg($dest, $filepathServeur.$name);

				imagedestroy($dest);
				imagedestroy($new_image);
				imagedestroy($source);

			}

			private function image_resized_dims($max_w, $max_h, $w, $h) {
				$ratio = $w / $h ;
				$ratio_max = $max_w / $max_h ;

				// calcul des hauteurs et largeurs finales
				if ( $ratio_max < $ratio ) {
					$new_w = $max_w ;
					$new_h = $max_w / $ratio ;
				}
				else {
					$new_w = $max_h * $ratio ;
					$new_h = $max_h ;
				}

				return array($new_w, $new_h) ;
			}
		}
	}